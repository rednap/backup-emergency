# backup bitbucket and git repos to ec2  
  
once configured, this will checkout all your repos from your ec2 instance, then it will rsync   
over ignored files from your local environment to your ec2. made to work on a stopped ec2 instance   
it will start the ec2 when you run the script, and it will stop the instance once the script finishes.    
if you ^c it will stop the instance, and if you decline at the prompt it will stop the instance.   
  
1. `cp config-emergency.ini.default ~/config-emergency.ini`  
  
2. edit `emergency` to reflect the path for your config

3. edit `repositories.txt` and `ignoredfiles.txt` to reflect your git repos  and files you wanna backup  
  
4. `cp repositories.txt ignoredfiles.txt ~/`  
  
5. `cp emergency ~/emergency` # or somewhere  in your PATH  

6. edit `~/config-emergency.ini` and enter your personal settings  
  
### note on setting up the EC2:   
- you will need to have python3 and pip3 installed.  
- you will need aws cli installed.  
- you will need git installed and configured.  
- you will need to have aws configured with an access id/key that has SSM access.  
- you'll need to make an ec2 role with ssm access and attach it to the instance you use.   
- if i forgot anything i'm sorry.  
